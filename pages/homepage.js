let homepage = function(){

    let firstNumber_input = element(by.model('first'));
    let secondNumber_input = element(by.model('second'));
    let goButton = element(by.css('[ng-click="doAddition()"]'));
    let selectOperationMultiplication = element(by.xpath(".//option[@value= 'MULTIPLICATION']"));
    let selectOperationAdition = element(by.xpath(".//option[@value= 'ADDITION']"));
    let selectOperationSubstraction = element(by.xpath(".//option[@value= 'SUBTRACTION']"));
    let selectOperationDivision = element(by.xpath(".//option[@value= 'DIVISION']"));


    let operator = element(by.model('operator'));


    this.getUrlProyect = function(url){
        browser.get(url);
    };

    this.enterFirstValueNumber = function (firstNumberValue) {
        firstNumber_input.sendKeys(firstNumberValue);
    };

    this.enterSecondValueNumber = function (secondNumberValue) {
        secondNumber_input.sendKeys(secondNumberValue);
    };

    this.selectOperation = function (operator) {

        switch (operator) {
            case "MULTIPLICATION":
             selectOperationMultiplication.click();
            break;

            case "ADDITION":
                selectOperationAdition.click();
            break;

            case "SUBTRACTION":
                selectOperationSubstraction.click();
            break;

            case "DIVISION":
                selectOperationDivision.click();
            break;

            default:
             text = "No value found";
        }

    };

    this.operatorIcon = function () {
        operator.click();
    };

    this.clickButtonGo = function () {
        goButton.click();
    };

    this.verifyResultTitle = function (resultTitle) {
        expect(resultTitle).toEqual('Super Calculator');
    };

    this.verifyResult = function (result) {
        let output =  element(by.cssContainingText('.ng-binding', result));
        expect(output.getText()).toEqual(result);
    };


};

module.exports = new homepage();
