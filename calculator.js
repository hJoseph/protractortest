// spec.js
let homepage = require('./pages/homepage');
let urlProyect = {url: "http://juliemr.github.io/protractor-demo/"}; // url del proyecto

describe('Inizializar  Calcalator Angular demo', function () {

    it('It should have a title', function () {
        browser.get(urlProyect.url);
        homepage.verifyResultTitle('Super Calculator');
        browser.sleep(2000);
    });

    it('It should have add ', function () {
        homepage.getUrlProyect(urlProyect.url)
        homepage.enterFirstValueNumber('5');
        homepage.enterSecondValueNumber('3');
        homepage.clickButtonGo();
        browser.sleep(2000)

    });

    it('It should show resul of add ', function () {
        homepage.verifyResult('8');
        browser.sleep(2000);
    });

    it('should multiply two integers', function () {
        homepage.getUrlProyect(urlProyect.url)
        homepage.enterFirstValueNumber('5');
        homepage.enterSecondValueNumber('3');
        homepage.operatorIcon();
        homepage.selectOperation("MULTIPLICATION");
        homepage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('15');
        browser.sleep(2000);


        homepage.enterFirstValueNumber('5');
        homepage.enterSecondValueNumber('3');
        homepage.operatorIcon();
        homepage.selectOperation("ADDITION");
        homepage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('8');
        browser.sleep(2000);

        homepage.enterFirstValueNumber('5');
        homepage.enterSecondValueNumber('3');
        homepage.operatorIcon();
        homepage.selectOperation("SUBTRACTION");
        homepage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('2');
        browser.sleep(2000);

        homepage.enterFirstValueNumber('12');
        homepage.enterSecondValueNumber('3');
        homepage.operatorIcon();
        homepage.selectOperation("DIVISION");
        homepage.clickButtonGo();
        expect(element(by.binding('latest')).getText()).toEqual('4');
        browser.sleep(2000);
    });
});
